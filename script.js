"use strict";

// task 1

function isPalindrome(str) {
    str = str.toLowerCase().replace(/[^a-zа-яё0-9]/g, '');
    return str === str.split('').reverse().join('');
}

const inputString = 'А роза упала на лапу Азора';
const result = isPalindrome(inputString);
console.log(`Чи є рядок "${inputString}" паліндромом? ${result}`);

// task 2

// function checkStringLength(str, maxLength) {
//     const trimmedStr = str.trim();
//     return trimmedStr.length <= maxLength;
// }

// const inputString = '   Hello, world!   ';
// const maxAllowedLength = 15;
// const result = checkStringLength(inputString, maxAllowedLength);

// if (result) {
//     console.log(`Рядок "${inputString}" меньше або дорівнює ${maxAllowedLength} символам.`);
// } else {
//     console.log(`Рядок "${inputString}" довший за ${maxAllowedLength} символів.`);
// }

// task 3

// function calculateAge() {
//     const birthDate = prompt('Введіть свою дату народження (у форматі YYYY-MM-DD):');
//     const currentDate = new Date();
//     const diffInMilliseconds = currentDate - new Date(birthDate);
//     const ageInMilliseconds = new Date(diffInMilliseconds).getFullYear() - 1970;
//     return ageInMilliseconds;
// }

// const userAge = calculateAge();
// console.log(`Ваш вік: ${userAge} років.`);


